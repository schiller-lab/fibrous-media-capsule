# Computational characterization of nonwoven fibrous media

## Overview

This capsule contains the data analysis for the morphological and hydraulic properties of computationally generated nonwoven fibrous media. The analysis includes the statistical properties of the pore space morphology and the dependence of the hydraulic permeability and tortuosity on geometric as well as microstructural quantities. Curve fitting is performed for various scaling relations available in the literature, and a new expression for the constriction factor in terms of mean pore and throat size is developed.

### License

The code contained in this capsule is released under the terms of the BSD-3-Clause license. The data contained in this capsule is released under the Creative Commons Attribution-ShareAlike (CC BY-SA) license. Please refer to the `LICENSE` files for specific provisions of the licenses.

## Quick start

The complete analysis is described in the Jupyter notebook `fibrous-media-analysis.ipynb`. This notebook is executed during a "Reproducible Run" and generates the figures and tables in the results folder. The notebook can also be launched in a Cloud Workstation for further exploration.

The directories in this capsule are organized as follows:

* code/ contains the notebook, execution script, and nbconvert templates
* data/ contains the datasets for computationally generated regular and random nonwoven fibrous media
* data/figures/ contains figures from external tools (e.g., Paraview) that are imported in the notebook

### Dependencies

The following Python packages are imported in the notebook:

- Jupyter
- NumPy
- SciPy
- Matplotlib
- Pillow
- pandas
- seaborn

These packages are provisioned through conda, see the `Dockerfile` for the versions used in this capsule. Note that Pillow requires Ghostscript to be installed for processing the included EPS graphics.

## Research abstract

Flow phenomena in porous media are relevant in many industrial applications including wastewater filtration, chemical separations, and oil recovery. Nonwoven fibrous membranes are widely used as filtration media, and tailoring the microstructure of the pore space is a key to improving filtration efficiency and preventing membrane fouling. However, predicting effective properties such as permeability and tortuosity for complex porous microstructures remains a challenging task. While a variety of empirical formulas have been developed for simple geometries and granular media, their accuracy in predicting the fluid transport in nonwoven fibrous media is limited as they do not incorporate the influence of random fiber arrangement and orientation on the structure-property relations. Here we present a computational framework for generating realistic random fibrous media with a wide range of porosities and systematically analyze the effect of pore size distribution on permeability and tortuosity. We determine the pore network by extracting a subnetwork of the watershed segmentation of pore space, which yields the statistical distribution of pore and throat sizes and their connectivity. We further conduct pore-scale lattice Boltzmann simulations to predict the fluid flow through nonwoven fibrous media and use the results to test the accuracy of semi-empirical scaling relations. Combining the morphological analysis of the pore space with pore-scale flow simulations enables us to determine the influence of the statistical pore size distribution on the effective fluid transport properties over a wide range of macroscopic porosities. The results reveal that the random distribution and orientation of fibers has a pronounced effect on the permeability and tortuosity of nonwoven fibrous media. We find that semi-empirical coefficients such as the Kozeny-Carman coefficient depend strongly on the statistical distribution of pores and throats. This dependence can be quantified as a constriction factor that incorporates the pore size distribution to yield accurate predictions of permeability of random fibrous media. The computational framework can be extended to include polydisperse fiber diameters and other parameters relevant to manufacturing processes for fibrous media. In combination with experimental imaging techniques such as X-ray computed tomography or scanning electron microscopy, the quantitative connection between microstructure and fluid transport paves the way to rapid characterization and design of porous media with tailored properties for filtration and separation applications.

## Acknowledgments

The `nbconvert` templates are adapted from work by [Julius Schulz and Alexander Schlaich](https://github.com/schlaicha/jupyter-publication-scripts).
