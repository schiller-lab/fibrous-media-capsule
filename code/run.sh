#!/bin/bash
set -e

ln -snf ../results ./fibrous-media-analysis_files

jupyter nbconvert --to latex \
    --ExecutePreprocessor.allow_errors=True \
    --ExecutePreprocessor.timeout=-1 \
    --FilesWriter.build_directory=. \
    --execute 'fibrous-media-analysis.ipynb'
